package ru.tsc.fuksina.tm.service;

import ru.tsc.fuksina.tm.api.repository.IUserRepository;
import ru.tsc.fuksina.tm.api.service.IUserService;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.model.User;
import ru.tsc.fuksina.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository>implements IUserService {

    public UserService(final IUserRepository repository) {
        super(repository);
    }

    @Override
    public User findOneByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findOneByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        return findOneByLogin(login) != null;
    }

    @Override
    public User findOneByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findOneByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findOneByEmail(email) != null;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findOneById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final User user = findOneById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
